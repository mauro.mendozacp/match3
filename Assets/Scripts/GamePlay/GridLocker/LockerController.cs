using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Match3.GamePlay.Entities;

namespace Match3.GamePlay
{
    public class LockerController : MonoBehaviour
    {
        public int Id { get; set; } = 0;
        public int Points { get; set; } = 0;
        public int Row { get; set; } = 0;
        public int Col { get; set; } = 0;

        [HideInInspector] public Rigidbody2D rigid;
        Animator animator;
        SpriteRenderer spriteRenderer;
        Color colorSelected = new Color(1f, 1f, 1f, 1f);
        Color colorNoSelected = new Color(0.5f, 0.5f, 0.5f, 1f);

        void Awake()
        {
            rigid = GetComponent<Rigidbody2D>();
            animator = GetComponent<Animator>();
            spriteRenderer = transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>();
        }

        public void Init(Locker locker, int id, int i, int j)
        {
            Id = id;
            Row = i;
            Col = j;
            Points = locker.points;
            spriteRenderer.sprite = locker.sprite;
        }

        public void Respawn(Locker locker, int id)
        {
            Id = id;
            Points = locker.points;
            spriteRenderer.sprite = locker.sprite;
        }

        public void MouseDown()
        {
            LockerManager.OnLockerClick?.Invoke(this);
        }

        public void MouseEnter()
        {
            LockerManager.OnLockerSelect?.Invoke(this);
        }

        public void ChangeSprite(bool selected)
        {
            spriteRenderer.color = selected ? colorSelected : colorNoSelected;
        }

        public LockerController GetLockerAround(Vector2 direction)
        {
            LayerMask lockerMask = 1 << gameObject.layer;

            Vector3 posDir = (direction * transform.localScale.x);
            Vector2 origin = transform.position + posDir;

            RaycastHit2D raycast2d = Physics2D.Raycast(origin, direction, transform.localScale.x / 2, lockerMask);
            if (raycast2d.collider != null)
            {
                LockerController auxLocker = raycast2d.collider.gameObject.GetComponent<LockerController>();

                if ((auxLocker.Id == Id))
                    return auxLocker;
            }

            return null;
        }

        public void MatchLockerAnimation()
        {
            animator.SetTrigger("Match");
        }

        public void SelectLockerAnimation(bool selected)
        {
            animator.SetBool("Select", selected);
        }

        public void StopMatchAnim()
        {
            LockerManager.OnStopLockerAnimation?.Invoke();
        }
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Match3.GamePlay.Entities;
using Match3.Audio;

namespace Match3.GamePlay
{
    public class LockerManager : MonoBehaviour
    {
        [Range(1, 35)]
        [SerializeField] int rows = 10;
        [Range(1, 15)]
        [SerializeField] int cols = 5;
        [SerializeField] int minLockerSelect = 3;
        [SerializeField] LockerPoolManager poolManager;
        [SerializeField] List<Locker> lockers = new List<Locker>();

        [HideInInspector] public List<LockerController> lockersSelectList = new List<LockerController>();
        List<LockerController> lockersGridList = new List<LockerController>();
        Vector2 minVelStopped = new Vector2(0.25f, 0.25f);
        GridView gridView;

        public float LockerWidth { get; set; } = 1f;
        public float LockerHeight { get; set; } = 1f;
        public float MaxWidth { get; set; } = 8f;
        public float MaxHeight { get; set; } = 8.75f;
        public float GridOffsetY { get; set; } = 1.25f;
        float lockerOffsetX = 0.05f;
        float waitStopLockers = 1.5f;
        int stopLockerAnimCant = 0;

        public static Action<LockerController> OnLockerClick;
        public static Action<LockerController> OnLockerSelect;
        public static Action OnLockerMatch;
        public static Action OnStopLockerAnimation;

        void Awake()
        {
            gridView = GetComponent<GridView>();

            OnLockerClick += LockerClick;
            OnLockerSelect += LockerSelect;
            OnLockerMatch += LockerMatchMove;
            OnLockerMatch += LockerMatchScore;
            OnStopLockerAnimation += LockerStopAnimation;
        }

        void Start()
        {
            Camera cam = Camera.main;
            MaxHeight = (cam.ViewportToWorldPoint(Vector2.one).y * 2f) - GridOffsetY;
            MaxWidth = (cam.ViewportToWorldPoint(Vector2.one).x * 2f) / 3;

            poolManager.Init(rows * cols);
            CreateLockers();
            gridView.Init();
        }

        void OnDestroy()
        {
            OnLockerClick -= LockerClick;
            OnLockerSelect -= LockerSelect;
            OnLockerMatch -= LockerMatchMove;
            OnLockerMatch -= LockerMatchScore;
            OnStopLockerAnimation -= LockerStopAnimation;
        }

        void CreateLockers()
        {
            if (lockers.Count > 1)
            {
                LockerWidth = MaxWidth / cols;
                LockerHeight = MaxHeight / rows;

                for (int i = 0; i < rows; i++)
                {
                    for (int j = 0; j < cols; j++)
                    {
                        GameObject lockerGO = poolManager.GetLockerFromPool();
                        LockerController locker = lockerGO.GetComponent<LockerController>();

                        int id = GetLockerId(i, j);
                        locker.Init(lockers[id], id, i, j);
                        lockersGridList.Add(locker);

                        Vector3 auxPos = lockerGO.transform.position;
                        auxPos.x = transform.position.x + (LockerWidth * j);
                        auxPos.y = transform.position.y + (LockerHeight * i);
                        lockerGO.transform.position = auxPos;

                        Vector3 auxScale = lockerGO.transform.localScale;
                        auxScale.x = LockerWidth - lockerOffsetX;
                        auxScale.y = LockerHeight;
                        lockerGO.transform.localScale = auxScale;

                        lockerGO.transform.parent = transform.GetChild(0).transform;
                    }
                }
            }
        }

        void LockerClick(LockerController locker)
        {
            ChangeLockersSprite(false);
            SelectLocker(locker);
        }

        void LockerSelect(LockerController locker)
        {
            if (!CheckLockerRepeat(locker))
            {
                if (CheckLockerAdjacent(locker))
                {
                    SelectLocker(locker);
                }
            }
        }

        void LockerMatchMove()
        {
            if (lockersSelectList.Count >= minLockerSelect)
            {
                Player.OnMatchMove();
            }
            else
            {
                foreach (LockerController l in lockersSelectList)
                {
                    l.SelectLockerAnimation(false);
                }
                AudioManager.Get().Play("nomatch");
            }
        }

        void LockerMatchScore()
        {
            if (lockersSelectList.Count >= minLockerSelect)
            {
                int points = 0;
                Player.OnMatchWait(true);

                foreach (LockerController l in lockersSelectList)
                {
                    points += l.Points;
                    l.SelectLockerAnimation(false);
                    l.MatchLockerAnimation();
                    ChangeLockerIndexPosition(l);
                }

                AudioManager.Get().Play("match");
                Player.OnMatchScore(points);
                ChangeLockersSprite(true);
            }
            else
            {
                ChangeLockersSprite(true);
                lockersSelectList.Clear();
            }
        }

        void ChangeLockerIndexPosition(LockerController locker)
        {
            foreach (LockerController l in lockersGridList)
            {
                if (l.Row > locker.Row && l.Col == locker.Col)
                {
                    l.Row--;
                }
            }
            locker.Row = rows - 1;
        }

        void RespawnLockers()
        {
            int[] contRow = new int[cols];
            foreach (LockerController lockerSelect in lockersSelectList)
            {
                GameObject lockerGO = poolManager.GetLockerFromPool();

                int id = GetLockerId(lockerSelect.Row, lockerSelect.Col);
                lockerSelect.Respawn(lockers[id], id);

                Vector3 auxPos = lockerGO.transform.position;
                auxPos.y = transform.position.y + MaxHeight + (contRow[lockerSelect.Col] * LockerHeight);
                lockerGO.transform.position = auxPos;

                contRow[lockerSelect.Col]++;
            }
        }

        bool CheckLockerAdjacent(LockerController locker)
        {
            if (lockersSelectList.Count > 0)
            {
                LockerController lastLocker = lockersSelectList[lockersSelectList.Count - 1];

                if (locker != lastLocker)
                {
                    if (locker.Id == lastLocker.Id)
                    {
                        int minRow = (lastLocker.Row - 1) >= 0 ? (lastLocker.Row - 1) : 0;
                        int minCol = (lastLocker.Col - 1) >= 0 ? (lastLocker.Col - 1) : 0;
                        int maxRow = (lastLocker.Row + 1) < rows ? (lastLocker.Row + 1) : rows;
                        int maxCol = (lastLocker.Col + 1) < cols ? (lastLocker.Col + 1) : cols;

                        if (locker.Row >= minRow && locker.Row <= maxRow && locker.Col >= minCol && locker.Col <= maxCol)
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        bool CheckLockerRepeat(LockerController locker)
        {
            for (int i = 0; i < lockersSelectList.Count; i++)
            {
                if (locker.Row == lockersSelectList[i].Row && locker.Col == lockersSelectList[i].Col)
                {
                    for (int j = i + 1; j < lockersSelectList.Count; j++)
                    {
                        lockersSelectList[j].ChangeSprite(false);
                        lockersSelectList[j].SelectLockerAnimation(false);
                    }

                    AudioManager.Get().Play("deselect");
                    lockersSelectList.RemoveRange(i + 1, lockersSelectList.Count - i - 1);
                    return true;
                }
            }

            return false;
        }

        int GetLockerId(int i, int j)
        {
            List<int> auxLockersId = new List<int>();
            int id = 0;
            int preRow = i - 1;
            int preCol = j - 1;
            int auxId1 = GetLockerGridId(preRow, j);
            int auxId2 = GetLockerGridId(i, preCol);

            for (int k = 0; k < lockers.Count; k++)
            {
                if (k != auxId1 && k != auxId2)
                {
                    auxLockersId.Add(k);
                }
            }

            int index = UnityEngine.Random.Range(0, auxLockersId.Count);
            id = auxLockersId[index];

            return id;
        }

        int GetLockerGridId(int i, int j)
        {
            int id = -1;

            if (i >= 0 && j >= 0)
            {
                foreach (LockerController l in lockersGridList)
                {
                    if (i == l.Row && j == l.Col)
                    {
                        id = l.Id;
                        break;
                    }
                }
            }

            return id;
        }

        void ChangeLockersSprite(bool selected)
        {
            foreach (LockerController l in lockersGridList)
            {
                l.ChangeSprite(selected);
            }
        }

        void SelectLocker(LockerController locker)
        {
            AudioManager.Get().Play("select");
            locker.ChangeSprite(true);
            locker.SelectLockerAnimation(true);
            lockersSelectList.Add(locker);
        }

        bool CheckLockerMatch()
        {
            foreach (LockerController locker in lockersGridList)
            {
                lockersSelectList.Add(locker);
                if (CheckLockerAroundDirection(locker, Vector2.right))
                    break;

                lockersSelectList.Add(locker);
                if (CheckLockerAroundDirection(locker, Vector2.up))
                    break;
            }

            if (lockersSelectList.Count >= minLockerSelect)
            {
                LockerMatchScore();
                return true;
            }
            else
                return false;
                
        }

        bool CheckLockerAroundDirection(LockerController locker, Vector2 direction)
        {
            LockerController auxLocker = locker.GetLockerAround(direction);

            if (auxLocker != null)
            {
                lockersSelectList.Add(auxLocker);
                return CheckLockerAroundDirection(auxLocker, direction);
            }

            if (lockersSelectList.Count >= minLockerSelect)
                return true;
            else
            {
                lockersSelectList.Clear();
                return false;
            }
        }

        public void ResetLockers()
        {
            foreach (LockerController locker in lockersGridList)
            {
                poolManager.ReturnLockerToPool(locker.gameObject);
            }
            lockersGridList.Clear();

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < cols; j++)
                {
                    GameObject lockerGO = poolManager.GetLockerFromPool();
                    LockerController locker = lockerGO.GetComponent<LockerController>();

                    int id = GetLockerId(i, j);
                    locker.Init(lockers[id], id, i, j);
                    lockersGridList.Add(locker);

                    Vector3 auxPos = lockerGO.transform.position;
                    auxPos.x = transform.position.x + (LockerWidth * j);
                    auxPos.y = transform.position.y + MaxHeight + (i * LockerHeight) + (j * LockerHeight);
                    lockerGO.transform.position = auxPos;
                }
            }

            StartCoroutine(WaitLockersInPosition());
        }

        void LockerStopAnimation()
        {
            stopLockerAnimCant++;
            if (stopLockerAnimCant >= lockersSelectList.Count - 1)
            {
                stopLockerAnimCant = 0;

                if (lockersSelectList.Count > 0)
                {
                    foreach (LockerController l in lockersSelectList)
                    {
                        poolManager.ReturnLockerToPool(l.gameObject);
                    }
                    RespawnLockers();
                    lockersSelectList.Clear();
                    StartCoroutine(WaitLockersInPosition());
                }
            }
        }

        IEnumerator WaitLockersInPosition()
        {
            yield return new WaitForSeconds(waitStopLockers);

            bool stopped = false;
            while (!stopped)
            {
                stopped = true;

                foreach (LockerController locker in lockersGridList)
                {
                    if (locker.rigid.velocity.magnitude > minVelStopped.magnitude)
                    {
                        stopped = false;
                        break;
                    }
                }

                yield return new WaitForFixedUpdate();
            }

            if (!CheckLockerMatch())
            {
                Player.OnMatchWait(false);
                GameplayManager.OnLockersStopped?.Invoke();
            }

            yield return null;
        }
    }
}
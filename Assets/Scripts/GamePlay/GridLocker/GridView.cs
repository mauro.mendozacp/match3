using UnityEngine;

namespace Match3.GamePlay
{
    public class GridView : MonoBehaviour
    {
        [SerializeField] GameObject floor;
        [SerializeField] GameObject background;

        LockerManager lockerManager;

        void Awake()
        {
            lockerManager = GetComponent<LockerManager>();
        }

        public void Init()
        {
            UpdateFloorPosition();
            UpdateBackgroundPosition();
            ResizeGrid();
        }

        void UpdateFloorPosition()
        {
            Vector3 auxPos = floor.transform.position;
            auxPos.x = transform.position.x + (lockerManager.MaxWidth / 2) - (lockerManager.LockerWidth / 2);
            auxPos.y = transform.position.y - (lockerManager.LockerHeight / 2);
            floor.transform.position = auxPos;
        }

        void UpdateBackgroundPosition()
        {
            Vector3 auxPos = background.transform.position;
            auxPos.x = transform.position.x + (lockerManager.MaxWidth / 2) - (lockerManager.LockerWidth / 2);
            auxPos.y = transform.position.y + (lockerManager.MaxHeight / 2) - (lockerManager.LockerHeight / 2);
            background.transform.position = auxPos;

            Vector3 auxScale = background.transform.localScale;
            auxScale.x = lockerManager.MaxWidth + (lockerManager.LockerWidth * 2);
            auxScale.y = lockerManager.MaxHeight + (lockerManager.LockerHeight / 2);
            background.transform.localScale = auxScale;
        }

        void ResizeGrid()
        {
            Vector3 auxPos = transform.position;
            auxPos.x = transform.position.x - (lockerManager.MaxWidth / 2) + (lockerManager.LockerWidth / 2);
            auxPos.y = transform.position.y - lockerManager.GridOffsetY / 2 - (lockerManager.MaxHeight / 2) + (lockerManager.LockerHeight / 2);
            transform.position = auxPos;
        }
    }
}
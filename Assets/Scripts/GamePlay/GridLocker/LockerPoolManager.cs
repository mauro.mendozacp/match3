using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Match3.GamePlay
{
    public class LockerPoolManager : MonoBehaviour
    {
        [SerializeField] GameObject lockerPrefab;
        int lockerSize;
        Queue<GameObject> lockerPool;

        public void Init(int size)
        {
            lockerPool = new Queue<GameObject>();

            for (int i = 0; i < size; i++)
            {
                GameObject lockerGO = Instantiate(lockerPrefab);
                lockerGO.name = "Locker" + (i + 1);
                lockerPool.Enqueue(lockerGO);
                lockerGO.SetActive(false);
            }
        }

        public GameObject GetLockerFromPool()
        {
            GameObject lockerGO = lockerPool.Dequeue();
            lockerGO.SetActive(true);
            return lockerGO;
        }

        public void ReturnLockerToPool(GameObject lockerGO)
        {
            lockerGO.SetActive(false);
            lockerPool.Enqueue(lockerGO);
        }
    }
}
using UnityEngine;
using Match3.GamePlay.Entities;
using TMPro;

namespace Match3.GamePlay.UI
{
    public class HUD : MonoBehaviour
    {
        [SerializeField] GameObject pauseGO;
        [SerializeField] TMP_Text movesText;
        [SerializeField] TMP_Text scoreText;

        void Awake()
        {
            Player.OnReceiveMoves += UpdateMovesText;
            Player.OnReceiveScore += UpdateScoreText;
        }

        void OnDestroy()
        {
            Player.OnReceiveMoves -= UpdateMovesText;
            Player.OnReceiveScore -= UpdateScoreText;
        }

        void UpdateMovesText(int moves)
        {
            movesText.text = "MOVES " + moves;
        }

        void UpdateScoreText(int score)
        {
            scoreText.text = score.ToString();
        }

        public void PauseGame()
        {
            GameplayManager.Get().Paused = true;
            pauseGO.SetActive(true);
            Time.timeScale = 0.0f;
        }
    }
}
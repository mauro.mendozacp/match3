using UnityEngine;
using Match3.Audio;

namespace Match3.UI
{
    public class ButtonUI : MonoBehaviour
    {
        public void Click()
        {
            AudioManager.Get().Play("click");
        }

        public void Hover()
        {
            AudioManager.Get().Play("hover");
        }
    }
}
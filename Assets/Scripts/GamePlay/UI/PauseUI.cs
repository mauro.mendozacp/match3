﻿using UnityEngine;
using UnityEngine.UI;
using Match3.Audio;

namespace Match3.GamePlay.UI
{
    public class PauseUI : MonoBehaviour
    {
        [SerializeField] Sprite sfxOn;
        [SerializeField] Sprite sfxOff;
        [SerializeField] Sprite musicOn;
        [SerializeField] Sprite musicOff;
        [SerializeField] GameObject sfxButton;
        [SerializeField] GameObject musicButton;

        public void Resume()
        {
            GameplayManager.Get().Paused = false;
            Time.timeScale = 1f;
            gameObject.SetActive(false);
        }

        public void MuteSfx()
        {
            bool currentMute = AudioManager.Get().sfxMuted;
            AudioManager.Get().MuteSFX(!currentMute);
            sfxButton.GetComponent<Image>().sprite = currentMute ? sfxOn : sfxOff;
        }

        public void MuteMusic()
        {
            bool currentMute = AudioManager.Get().musicMuted;
            AudioManager.Get().MuteMusic(!currentMute);
            musicButton.GetComponent<Image>().sprite = currentMute ? musicOn : musicOff;
        }
    }
}
using System;
using UnityEngine;
using Match3.GamePlay.Entities;
using Match3.Audio;

namespace Match3.GamePlay
{
    public class GameplayManager : MonoBehaviourSingleton<GameplayManager>
    {
        [SerializeField] LockerManager lockerManager;
        [SerializeField] Player player;
        [SerializeField] GameObject resetGridPanel;

        public bool Paused { get; set; } = false;
        bool finishLevel = false;

        public static Action OnLockersStopped;

        public override void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
        }

        void Start()
        {
            AudioManager.Get().Play("gameplaytheme");
            OnLockersStopped += LockerStopped;
        }

        void OnDestroy()
        {
            OnLockersStopped -= LockerStopped;
        }

        void LockerStopped()
        {
            if (!finishLevel)
            {
                if (player.CurrentMoves == 0)
                {
                    finishLevel = true;
                    lockerManager.ResetLockers();
                    resetGridPanel.SetActive(true);
                    AudioManager.Get().AccelerateMusicGameplay(false);
                }
            }
            else
            {
                finishLevel = false;
                player.CurrentMoves = player.moves;
                resetGridPanel.SetActive(false);
            }
        }
    }
}
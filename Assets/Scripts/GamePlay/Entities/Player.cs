using System;
using UnityEngine;
using Match3.Audio;

namespace Match3.GamePlay.Entities
{
    public class Player : MonoBehaviour
    {
        public int moves = 1;
        [SerializeField] LayerMask lockerMask;

        Collider2D lastLockerCollider2d = null;
        int currentMoves = 0;
        int score = 0;

        public bool Wait { get; set; } = false;
        public bool Pressed { get; set; } = false;
        public int Score
        {
            get => score;
            set
            {
                score = value;
                OnReceiveScore?.Invoke(score);
            }
        }

        public int CurrentMoves
        {
            get => currentMoves;
            set
            {
                currentMoves = (value < 0) ? 0 : value;
                OnReceiveMoves?.Invoke(currentMoves);

                if (currentMoves <= moves / 4)
                    AudioManager.Get().AccelerateMusicGameplay(true);
            }
        }

        public static Action OnMatchMove;
        public static Action<int> OnMatchScore;
        public static Action<int> OnReceiveMoves;
        public static Action<int> OnReceiveScore;
        public static Action<bool> OnMatchWait;

        void Start()
        {
            OnMatchScore += MatchScore;
            OnMatchMove += MatchMove;
            OnMatchWait += MatchWait;
            CurrentMoves = moves;
        }

        void Update()
        {
            if (!GameplayManager.Get().Paused)
            {
                if (CurrentMoves > 0 && !Wait)
                {
                    LockerClick();
                    LockerSelect();
                    LockerMatch();
                }
            }
        }

        void OnDestroy()
        {
            OnMatchScore -= MatchScore;
            OnMatchMove -= MatchMove;
            OnMatchWait -= MatchWait;
        }

        void LockerClick()
        {
            if (Input.GetMouseButtonDown(0))
            {
                RaycastHit2D hit2d = GetRaycast2DMouse();

                if (hit2d.collider != null)
                {
                    Pressed = true;
                    lastLockerCollider2d = hit2d.collider;
                    hit2d.collider.gameObject.GetComponent<LockerController>().MouseDown();
                }
            }
        }

        void LockerSelect()
        {
            if (Pressed)
            {
                if (Input.GetMouseButton(0))
                {
                    RaycastHit2D hit2d = GetRaycast2DMouse();

                    if (hit2d.collider != null && lastLockerCollider2d != hit2d.collider)
                    {
                        lastLockerCollider2d = hit2d.collider;
                        hit2d.collider.gameObject.GetComponent<LockerController>().MouseEnter();
                    }
                }
            }
        }

        void LockerMatch()
        {
            if (Pressed)
            {
                if (Input.GetMouseButtonUp(0))
                {
                    Pressed = false;
                    LockerManager.OnLockerMatch?.Invoke();
                }
            }
        }

        RaycastHit2D GetRaycast2DMouse()
        {
            Vector2 inputPos = Input.mousePosition;
            Ray rayMouse = Camera.main.ScreenPointToRay(inputPos);
            return Physics2D.Raycast(rayMouse.origin, rayMouse.direction, Mathf.Infinity, lockerMask);
        }

        void MatchScore(int points)
        {
            Score += points;
        }

        void MatchMove()
        {
            CurrentMoves--;
        }

        void MatchWait(bool wait)
        {
            Wait = wait;
        }
    }
}
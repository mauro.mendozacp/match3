using UnityEngine;

namespace Match3.GamePlay.Entities
{
    [CreateAssetMenu(fileName = "New Locker", menuName = "Entities/Locker")]
    public class Locker : ScriptableObject
    {
        public int points = 0;
        public Sprite sprite;
    }
}